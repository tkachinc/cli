<?php
namespace TkachInc\CLI;

use ArrayIterator;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ArgumentParser
{
	/**
	 * @var ArrayIterator
	 */
	protected $argv;

	/**
	 * @var Arguments
	 */
	protected $arguments;

	/**
	 * @var array
	 */
	protected $shortAttrs = [];

	/**
	 * @var array
	 */
	protected $attrs = [];

	/**
	 * @var
	 */
	protected $callbacks = [];

	/**
	 * @var string
	 */
	protected $filename;

	/**
	 * @param Arguments $arguments
	 */
	public function __construct(Arguments $arguments)
	{
		$this->arguments = $arguments;
	}

	/**
	 * @param array $argv
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public function setArgv(Array $argv)
	{
		if (empty($argv)) {
			throw new \Exception('Argv is empty');
		}
		$this->filename = array_shift($argv);
		$this->setArgument('filename', $this->filename);
		$this->argv = new ArrayIterator($argv);

		return $this;
	}

	/**
	 * @param array $shortAttrs
	 *
	 * @return $this
	 */
	public function setShortAttrs(Array $shortAttrs = [])
	{
		if (!empty($shortAttrs)) {
			$this->shortAttrs = $shortAttrs;
		}

		return $this;
	}

	/**
	 * @param array $shortAttrs
	 *
	 * @return $this
	 */
	public function appendShortAttrs(Array $shortAttrs = [])
	{
		if (!empty($shortAttrs)) {
			$this->shortAttrs = array_merge($this->shortAttrs, $shortAttrs);
		}

		return $this;
	}

	/**
	 * @param array $attrs
	 *
	 * @return $this
	 */
	public function setAttrs(Array $attrs = [])
	{
		if (!empty($attrs)) {
			$this->attrs = $attrs;
		}

		return $this;
	}

	/**
	 * @param array $attrs
	 *
	 * @return $this
	 */
	public function appendAttrs(Array $attrs = [])
	{
		if (!empty($attrs)) {
			$this->attrs = array_merge($this->attrs, $attrs);
		}

		return $this;
	}

	/**
	 * @param      $key
	 * @param      $value
	 * @param null $callback
	 *
	 * @return $this
	 */
	public function addAttr($key, $value, $callback = null)
	{
		$this->attrs[$key] = $value;

		if ($callback) {
			$this->callbacks[$key] = $callback;
		}

		return $this;
	}

	/**
	 * @param $key
	 * @param $value
	 *
	 * @return $this
	 */
	public function addShortAttr($key, $value)
	{
		$this->shortAttrs[$key] = $value;

		return $this;
	}

	/**
	 * @param $key
	 *
	 * @return $this
	 */
	public function remAttr($key)
	{
		if (isset($this->attrs[$key])) {
			unlink($this->attrs[$key]);
		}

		return $this;
	}

	/**
	 * @param $key
	 *
	 * @return $this
	 */
	public function remShortAttr($key)
	{
		if (isset($this->shortAttrs[$key])) {
			unlink($this->shortAttrs[$key]);
		}

		return $this;
	}

	/**
	 * @return Arguments
	 * @throws \Exception
	 */
	public function parse()
	{
		if (!isset($this->argv)) {
			throw new \Exception('Not found argv');
		}
		if (!isset($this->arguments)) {
			throw new \Exception('Not found arguments');
		}
		$this->argv->rewind();
		while (true) {
			if ($this->argv->key() === null) {
				break;
			}

			$attr = $this->argv->current();
			if (isset($this->shortAttrs[$attr])) {
				$attr = $this->shortAttrs[$attr];
			}

			if (substr_count($attr, '=') >= 1) {
				$tmp = explode('=', $attr);
				$attr = array_shift($tmp);
				$val = implode('=', $tmp);
				$this->argv->append($attr);
				$this->argv->append($val);
				$this->argv->next();
				continue;
			}

			$attrLen = strlen($attr);
			if (substr_count($attr, '-') === 1 && $attrLen > 2) {
				$i = 1;
				while ($i < $attrLen) {
					$this->argv->append('-' . substr($attr, $i, 1));
					$i++;
				}
				$this->argv->next();
				continue;
			}

			$clearAttr = str_replace('-', '', $attr);
			if (isset($this->attrs[$clearAttr])) {

				if (is_callable($this->attrs[$clearAttr])) {
					$this->setArgument($clearAttr, $this->attrs[$clearAttr]);
				} elseif (is_array($this->attrs[$clearAttr])) {
					$array = [];

					foreach ($this->attrs[$clearAttr] as $key => $step) {
						$seek = $this->argv->key() + $step;
						if ($seek + 1 > $this->argv->count()) {
							$array[$key] = null;
							continue;
						}
						$this->argv->seek($seek);
						$array[$key] = $this->argv->current();
					}
					$this->setArgument($clearAttr, $array);
					unset($array);
				} elseif (is_int($this->attrs[$clearAttr])) {
					$seek = $this->argv->key() + $this->attrs[$clearAttr];

					if ($seek + 1 > $this->argv->count()) {
						$this->setArgument($clearAttr, null);
					} else {
						$this->argv->seek($seek);
						$this->setArgument($clearAttr, $this->argv->current());
					}
				} else {
					$this->setArgument($clearAttr, $this->attrs[$clearAttr]);
				}
			}
			$this->argv->next();
		}

		return $this->arguments;
	}

	/**
	 * @param string $key
	 * @param mixed  $value
	 *
	 * @return $this
	 */
	public function setArgument($key, $value)
	{
		if (isset($this->callbacks[$key]) && is_callable($this->callbacks[$key])) {
			call_user_func_array($this->callbacks[$key], [&$value]);
		}

		$this->arguments->setMulti($key, $value);

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFilename()
	{
		return $this->filename;
	}

	/**
	 * @return Arguments
	 */
	public function getArguments()
	{
		return $this->arguments;
	}
}