<?php
namespace TkachInc\CLI\Process;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ProcessHelper
{
	/**
	 * @param string|int $pid
	 *
	 * @return float
	 */
	public static function getProcessCPUUsage($pid)
	{
		return (float)shell_exec('ps -p ' . $pid . ' -o pcpu | grep -v CPU');
	}

	/**
	 * @param string $name
	 * @param array  $without
	 * @param null   $limit
	 *
	 * @return string
	 */
	public static function getProcessPidByName($name, Array $without = [], $limit = null)
	{
		$command = 'ps axf | grep ' . $name . ' ';
		if (empty($without)) {
			$without = ['grep', 'sh'];
		}
		foreach ($without as $str) {
			$command .= '| grep -v ' . $str . ' ';
		}
		$command .= '| awk \'{print $1}\' ';
		if (is_int($limit) && $limit >= 1) {
			$command .= '| head -n ' . $limit . ' ';
		}

		return shell_exec($command);
	}

	/**
	 * @param int|string $pid
	 *
	 * @return string
	 */
	public static function issetProcess($pid)
	{
		return (shell_exec('ps -p ' . $pid . ' -o pcpu | grep -v CPU') !== null);
	}

	/**
	 * @param int|string $pid
	 *
	 * @return string
	 */
	public static function getProcessName($pid)
	{
		return shell_exec('ps -p ' . $pid . ' -o command | grep -v COMMAND');
	}

	/**
	 * @param int|string $pid
	 *
	 * @return string
	 */
	public static function getMemoryUsageByCli($pid)
	{
		return (int)shell_exec('ps -p ' . $pid . ' -o rss | grep -v RSS');
	}

	/**
	 * @param int|string $pid
	 */
	public static function stopProcessByPid($pid)
	{
		static::killWithSigProcessByPid($pid, 19); //SIGSTOP
	}

	/**
	 * @param int|string $pid
	 */
	public static function startProcessByPid($pid)
	{
		static::killWithSigProcessByPid($pid, 18); //SIGSTART
	}

	/**
	 * @return int
	 */
	public static function getCoresInSystem()
	{
		return (int)shell_exec('grep -c \'^processor\' /proc/cpuinfo');
	}

	/**
	 * @param null $real
	 *
	 * @return int
	 */
	public static function getMemoryUsage($real = null)
	{
		return memory_get_usage($real);
	}

	/**
	 * @param int $size
	 *
	 * @return string
	 */
	public static function convert($size)
	{
		$unit = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
		if ($size <= 0) {
			return '0B';
		}

		return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . '' . $unit[$i];
	}

	/**
	 * @return array
	 */
	public static function getCPULoadAvg()
	{
		return sys_getloadavg();
	}

	/**
	 * @param int|string $pid
	 */
	public static function killProcessByPid($pid)
	{
		static::killWithSigProcessByPid($pid, 9);
	}

	/**
	 * @param int|string $pid
	 */
	public static function sigTermProcessByPid($pid)
	{
		static::killWithSigProcessByPid($pid, 15);
	}

	/**
	 * @param int|string $pid
	 * @param int        $sig
	 */
	public static function killWithSigProcessByPid($pid, $sig = 15)
	{
		shell_exec('kill -' . $sig . ' ' . $pid);
	}

	/**
	 * Функция совместима по аргументам с system() в С
	 *
	 * @param string $cmd
	 *
	 * @return int
	 */
	public static function cSystem($cmd)
	{
		$pp = proc_open($cmd, [STDIN, STDOUT, STDERR], $pipes);
		if (!$pp) {
			return 127;
		}

		return proc_close($pp);
	}

	/**
	 * @param string $cmd
	 */
	public static function execute($cmd)
	{
		exec($cmd);
	}

	/**
	 * @param array $dirs
	 * @param array $suffixes
	 * @param null  $php
	 *
	 * @return null|string
	 */
	public static function getPHPBinary(Array $dirs = [], Array $suffixes = [], $php = null)
	{
		if ($php != null && is_executable($php)) {
			return $php;
		}

		$isHHVM = defined('HHVM_VERSION');
		$arguments = [];
		if ($isHHVM) {
			$arguments[] = '--php';
		} elseif ('phpdbg' === PHP_SAPI) {
			$arguments[] = '-qrr';
		}
		$args = '';
		if (!empty($arguments)) {
			$args = ' ' . implode(' ', $arguments);
		}

		if ($isHHVM) {
			return (getenv('PHP_BINARY') ?: PHP_BINARY) . $args;
		}

		if (PHP_BINARY && in_array(PHP_SAPI, ['cli', 'cli-server', 'phpdbg']) && is_file(PHP_BINARY)) {
			return PHP_BINARY . $args;
		}

		if ($php = getenv('PHP_PATH')) {
			if (!is_executable($php)) {
				return false;
			}

			return $php;
		}

		if ($php = getenv('PHP_PEAR_PHP_BIN')) {
			if (is_executable($php)) {
				return $php;
			}
		}

		$dirs[] = PHP_BINDIR;
		if (ini_get('open_basedir')) {
			$searchPath = explode(PATH_SEPARATOR, ini_get('open_basedir'));
			$dirs = [];
			foreach ($searchPath as $path) {
				// Silencing against https://bugs.php.net/69240
				if (@is_dir($path)) {
					$dirs[] = $path;
				} else {
					if (basename($path) == 'php' && is_executable($path)) {
						return $path;
					}
				}
			}
		} else {
			$dirs = array_merge(
				explode(PATH_SEPARATOR, getenv('PATH') ?: getenv('Path')),
				$dirs
			);
		}

		$suffixes[] = '';
		foreach ($suffixes as $suffix) {
			foreach ($dirs as $dir) {
				if (is_file($file = $dir . DIRECTORY_SEPARATOR . 'php' . $suffix) && (is_executable($file))) {
					return $file;
				}
			}
		}

		return null;
	}

	/**
	 * @return string
	 */
	public static function getTempDir()
	{
		if (function_exists('sys_get_temp_dir')) {
			$tmp = sys_get_temp_dir();
		} elseif (!empty($_SERVER['TMP'])) {
			$tmp = $_SERVER['TMP'];
		} elseif (!empty($_SERVER['TEMP'])) {
			$tmp = $_SERVER['TEMP'];
		} elseif (!empty($_SERVER['TMPDIR'])) {
			$tmp = $_SERVER['TMPDIR'];
		} else {
			$tmp = getcwd();
		}

		return $tmp;
	}

	/**
	 * @param     $pid
	 * @param int $maxCPUUsage
	 * @param int $microseconds
	 * @param int $slowMax
	 * @param int $slowSleep
	 */
	public static function balanceProcessByPid($pid,
	                                           $maxCPUUsage = 50,
	                                           $microseconds = 10000,
	                                           $slowMax = 100,
	                                           $slowSleep = 1)
	{
		if ($pid !== getmypid() && static::getProcessCPUUsage($pid) > $maxCPUUsage) {
			static::stopProcessByPid($pid);
			$e = 0;
			while (static::getProcessCPUUsage($pid) > $maxCPUUsage) {
				$e++;
				usleep($microseconds);
				if ($e > $slowMax) {
					$e = 0;
					sleep($slowSleep);
				}
			}
			static::startProcessByPid($pid);
		}
	}

	/**
	 * @param bool $withoutCurrentProcess
	 * @param bool $withoutSH
	 * @param bool $includeHead
	 *
	 * @return \Generator
	 */
	public static function getAllProcess($withoutCurrentProcess = true, $withoutSH = true, $includeHead = false)
	{
		$cmd = 'ps -aux';

		if ($withoutCurrentProcess) {
			$cmd .= ' | grep -v ps';
		}
		if ($withoutSH) {
			$cmd .= ' | grep -v "sh -c"';
		}

		$keys = [
			'user',
			'pid',
			'cpu_p',
			'mem_p',
			'vsz',
			'rss',
			'tty',
			'stat',
			'start',
			'time',
			'command',
		];
		$args = [
			'USER',
			'PID',
			'%CPU',
			'%MEM',
			'VSZ',
			'RSS',
			'TTY',
			'STAT',
			'START',
			'TIME',
			'COMMAND',
		];
		$pattern = "%s %d %f %f %d %d %s %s %s %s %[^\n]s";
		$output = rtrim(shell_exec($cmd), PHP_EOL);
		$processList = explode(PHP_EOL, $output);

		if ($includeHead) {
			yield array_combine($keys, $args);
		}

		foreach ($processList as $process) {
			while (strpos($process, '  ') !== false) {
				$process = str_replace('  ', ' ', $process);
			}
			sscanf($process, $pattern, ...$args);
			yield array_combine($keys, $args);
		}
	}
}