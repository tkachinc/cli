<?php
namespace TkachInc\CLI;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class GetoptWrapper
{
	/**
	 * @var Arguments
	 */
	protected $arguments;

	/**
	 * @var array
	 */
	protected $attrs = [];

	/**
	 * @var array
	 */
	protected $values = [];

	/**
	 * @var array
	 */
	protected $shorts = [];

	/**
	 * @example
	 * $options = [ ['short' => 'r::', 'long' => 'route::'] ]; // r: - optional r:: - required r - empty
	 *
	 * @param array $options
	 *
	 * @return array
	 */
	public static function parse(Array $options)
	{
		$str = '';
		$long = [];

		foreach ($options as $key => $item) {
			if (isset($item['short'])) {
				$str .= $item['short'];
			}
			if (isset($item['long'])) {
				$long[] = $item['long'];
			}
		}

		return getopt($str, $long);
	}

	/**
	 * @param Arguments $arguments
	 */
	public function __construct(Arguments $arguments)
	{
		$this->arguments = $arguments;
	}

	/**
	 * @param      $short
	 * @param      $long
	 * @param      $value
	 * @param bool $required
	 * @param null $callback
	 *
	 * @return $this
	 */
	public function addAttr($short, $long, $value, $required = false, $callback = null)
	{
		if ($value !== 0 && $required) {
			$pshort = $short . ':';
			$plong = $long . ':';
		} else {
			$pshort = $short . '::';
			$plong = $long . '::';
		}

		$this->shorts[$short] = $long;
		$this->attrs[$long] = ['short' => $pshort, 'long' => $plong];
		$this->values[$long] = ['value' => $value, 'callback' => $callback];

		return $this;
	}

	/**
	 * Run parse
	 */
	public function run()
	{
		$result = static::parse(array_values($this->attrs));

		foreach ($result as $key => $value) {

			if (isset($this->values[$key])) {
				$this->worker($this->values[$key], $key, $value);
			} elseif (isset($this->shorts[$key])) {
				$long = $this->shorts[$key];
				$this->worker($this->values[$long], $long, $value);
			}
		}

		return $this->arguments;
	}

	/**
	 * @param $item
	 * @param $key
	 * @param $value
	 */
	protected function worker($item, $key, $value)
	{
		if (isset($item['callback']) && is_callable($item['callback'])) {
			call_user_func_array($item['callback'], [$value]);
		}

		if (is_int($item['value']) && $item['value'] > 0) {
			$this->arguments->setMulti($key, $value);
		} elseif (is_int($item['value'])) {
			$this->arguments->setMulti($key, null);
		} else {
			$this->arguments->setMulti($key, $item['value']);
		}
	}
}