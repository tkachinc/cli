<?php
namespace TkachInc\CLI;

use SeekableIterator;
use ArrayAccess;
use Countable;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Arguments implements SeekableIterator, Countable, ArrayAccess
{
	/**
	 * @var int
	 */
	protected $current = 0;

	/**
	 * @var array
	 */
	protected $keys = [];

	/**
	 * @var array
	 */
	protected $storage = [];

	/**
	 * @var array
	 */
	protected $keysCount = [];

	/**
	 * @var bool
	 */
	protected $callCallable = true;

	/**
	 * @param bool $callCallable
	 *
	 * @return $this
	 */
	public function setCallCallable(bool $callCallable)
	{
		$this->callCallable = $callCallable;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function getCallCallable()
	{
		return $this->callCallable;
	}

	/**
	 * @return \Generator
	 */
	public function generator()
	{
		reset($this->storage);

		foreach ($this->keys as $key) {
			yield $key => $this->get($key);
		}
	}

	/**
	 * @param $key
	 *
	 * @return $this
	 */
	protected function incKeysCount($key)
	{
		if (isset($this->keysCount[$key])) {
			$this->keysCount[$key]++;
		} else {
			$this->keysCount[$key] = 1;
		}

		return $this;
	}

	/**
	 * @param $key
	 *
	 * @return $this
	 */
	public function delete($key)
	{
		if (isset($this->keysCount[$key])) {
			unset($this->keysCount[$key]);
		}

		if (isset($this->storage[$key])) {
			unset($this->storage[$key]);
		}

		$key = array_search($key, $this->keys);
		if ($key !== false) {
			unset($this->keys[$key]);
		}

		return $this;
	}

	/**
	 * @param string $key
	 * @param mixed  $value
	 *
	 * @return $this
	 */
	public function set($key, $value)
	{
		$this->incKeysCount($key);

		$this->storage[$key] = $value;

		$this->keys[] = $key;

		return $this;
	}

	/**
	 * @param string $key
	 * @param mixed  $value
	 *
	 * @return $this
	 */
	public function setMulti($key, $value)
	{
		$this->incKeysCount($key);

		if (isset($this->storage[$key]) && !is_array($this->storage[$key])) {
			$this->storage[$key][] = $this->storage[$key];
		}

		if (isset($this->storage[$key]) && is_array($this->storage[$key])) {
			$this->storage[$key][] = $value;
		} else {
			$this->set($key, $value);
		}

		return $this;
	}

	/**
	 * @param $key
	 *
	 * @return int
	 */
	public function getKeyCount($key)
	{
		return isset($this->keysCount[$key]) ? $this->keysCount[$key] : 0;
	}

	/**
	 * @param string $key
	 * @param mixed  $default
	 *
	 * @return mixed
	 */
	public function get($key, $default = null)
	{
		if (isset($this->storage[$key])) {
			if (is_callable($this->storage[$key]) && $this->getCallCallable()) {
				$this->storage[$key] = call_user_func_array($this->storage[$key], [$this, $key]);
			}

			return $this->storage[$key];
		}

		return $default;
	}

	/**
	 * @param $name
	 *
	 * @return null
	 */
	public function __get($name)
	{
		return $this->get($name);
	}

	/**
	 * @return int
	 */
	public function count()
	{
		return count($this->storage);
	}

	/**
	 * @param mixed $offset
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function offsetExists($offset)
	{
		return array_key_exists($offset, $this->storage);
	}

	/**
	 * @param mixed $offset
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function offsetGet($offset)
	{
		return $this->get($offset);
	}

	/**
	 * @param mixed $offset
	 * @param mixed $value
	 *
	 * @throws \Exception
	 */
	public function offsetSet($offset, $value)
	{
		$this->set($offset, $value);
	}

	/**
	 * @param mixed $offset
	 *
	 * @throws \Exception
	 */
	public function offsetUnset($offset)
	{
		$this->delete($offset);
	}

	/**
	 * To next iteration
	 */
	public function next()
	{
		$this->current++;
	}

	/**
	 * @return int
	 */
	public function key()
	{
		return $this->current;
	}

	/**
	 * @return bool
	 */
	public function valid()
	{
		return $this->current <= $this->count();
	}

	/**
	 * Rewind
	 */
	public function rewind()
	{
		$this->current = 0;
	}

	/**
	 * @param int $position
	 */
	public function seek($position)
	{
		$this->current += $position;
	}

	/**
	 * @return mixed
	 */
	public function current()
	{
		$key = $this->keys[$this->current];

		return $this->get($key);
	}

	/**
	 * @return string
	 * @throws \Exception
	 */
	public function getPassword()
	{
		$command = "/usr/bin/env bash -c 'echo OK'";
		if (rtrim(shell_exec($command)) !== 'OK') {
			throw new \Exception("Can't invoke bash");
		}
		$command = "/usr/bin/env bash -c 'read -s -p \""
			. addslashes('Enter Password:')
			. "\" mypassword && echo \$mypassword'";
		$password = rtrim(shell_exec($command));
		echo "\n";
		return $password;
	}
}