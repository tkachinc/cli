<?php
namespace TkachInc\CLI\Locker;

use TkachInc\CLI\Process\ProcessHelper;

/**
 * @author maxim
 */
class ProcessLocker
{
	/**
	 * @var null|string
	 */
	protected $dir;

	/**
	 * ProcessLocker constructor.
	 *
	 * @param null $dir
	 */
	public function __construct($dir = null)
	{
		if (!$dir || !file_exists($dir)) {
			$dir = ProcessHelper::getTempDir();
		}

		$this->dir = $dir;
	}

	/**
	 * @param array $params
	 *
	 * @return string
	 */
	public static function getPidFilename(Array $params = [])
	{
		return 'lock_' . sha1(implode('', $params)) . '.pid';
	}

	/**
	 * @param string $lockFilename
	 *
	 * @return $this
	 */
	public function create($lockFilename)
	{
		$dir = $this->dir . '/pids';
		if (!file_exists($dir)) {
			mkdir($dir);
		}

		$pidFile = $dir . '/' . $lockFilename;
		file_put_contents($pidFile, getmypid());

		return $this;
	}

	/**
	 * @param string $lockFilename
	 *
	 * @return null|string
	 */
	public function get($lockFilename)
	{
		$temp = $this->dir . '/pids';
		if (!file_exists($temp)) {
			mkdir($temp);

			return null;
		}

		$pidFile = $temp . '/' . $lockFilename;
		if (file_exists($pidFile)) {
			return file_get_contents($pidFile);
		}

		return null;
	}

	/**
	 * @param string $lockFilename
	 *
	 * @return $this
	 */
	public function unlink($lockFilename)
	{
		$temp = $this->dir . '/pids';
		if (!file_exists($temp)) {
			mkdir($temp);

			return $this;
		}

		$pidFile = $temp . '/' . $lockFilename;
		if (file_exists($pidFile)) {
			unlink($pidFile);
		}

		return $this;
	}

	/**
	 * @param string $filename
	 * @param string $lockFilename
	 *
	 * @return bool
	 */
	public function needStop($filename, $lockFilename)
	{
		$pid = $this->get($lockFilename);
		if ($pid !== null && ProcessHelper::issetProcess($pid)) {
			$name = ProcessHelper::getProcessName($pid);
			if (strpos($name, $filename) !== false) {
				return true;
			}
		}

		return false;
	}
}