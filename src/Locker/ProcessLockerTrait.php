<?php
namespace TkachInc\CLI\Locker;

/**
 * @author maxim
 */
trait ProcessLockerTrait
{
	/**
	 * @param string $filename
	 * @param null   $route
	 * @param null   $args
	 *
	 * @return bool
	 */
	protected function lock($filename, $route = null, $args = null)
	{
		$lockFilename = PidFile::getPidFilename([$filename, $route, $args]);
		$lock = new PidFile();
		if ($lock->needStop($filename, $lockFilename)) {
			return true;
		}
		$lock->createPid($lockFilename);
		$shutdown = function () use ($lock, $lockFilename) {
			$lock->unlinkPid($lockFilename);
		};
		register_shutdown_function($shutdown);

		return false;
	}
}