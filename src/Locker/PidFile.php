<?php
namespace TkachInc\CLI\Locker;

use TkachInc\CLI\Process\ProcessHelper;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PidFile
{
	/**
	 * @param array $params
	 *
	 * @return string
	 */
	public static function getPidFilename(Array $params = [])
	{
		return 'lock_' . sha1(implode('', $params)) . '.pid';
	}

	/**
	 * @param string $lockFilename
	 *
	 * @return string|null
	 */
	public static function getPid($lockFilename)
	{
		$temp = ProcessHelper::getTempDir();
		$temp = $temp . '/pids';
		if (!file_exists($temp)) {
			mkdir($temp);

			return null;
		}

		$pidFile = $temp . '/' . $lockFilename;
		if (file_exists($pidFile)) {
			return file_get_contents($pidFile);
		}

		return null;
	}

	/**
	 * @param string $lockFilename
	 */
	public static function unlinkPid($lockFilename)
	{
		$temp = ProcessHelper::getTempDir();
		$temp = $temp . '/pids';
		if (!file_exists($temp)) {
			mkdir($temp);

			return;
		}

		$pidFile = $temp . '/' . $lockFilename;
		if (file_exists($pidFile)) {
			unlink($pidFile);
		}
	}

	/**
	 * @param string $lockFilename
	 */
	public static function createPid($lockFilename)
	{
		$temp = ProcessHelper::getTempDir();
		$temp = $temp . '/pids';
		if (!file_exists($temp)) {
			mkdir($temp);
		}

		$pidFile = $temp . '/' . $lockFilename;
		file_put_contents($pidFile, getmypid());
	}

	/**
	 * @param string $filename
	 * @param string $lockFilename
	 *
	 * @return bool
	 */
	public static function needStop($filename, $lockFilename)
	{
		$pid = static::getPid($lockFilename);
		if ($pid !== null && ProcessHelper::issetProcess($pid)) {
			$name = ProcessHelper::getProcessName($pid);
			if (strpos($name, $filename) !== false) {
				return true;
			}
		}

		return false;
	}
}