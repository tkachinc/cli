<?php
namespace TkachInc\CLI\Output;

/**
 * Class OutputCliHelper
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class LinesManipulator
{
	/**
	 * @var int
	 */
	protected static $lastLines = 0;

	/**
	 * @var int
	 */
	protected static $page = 0;

	/**
	 * @var int
	 */
	protected $lastLine = 0;

	/**
	 * @var string
	 */
	protected $data;

	/**
	 * @var string
	 */
	protected $temp;

	/**
	 * @return string
	 */
	public function getCommand()
	{
		stream_set_blocking(STDIN, false);
		$command = fgetc(STDIN);
		if ($command) {
			$magic = "\r\033[K\033[1A\r\033[K\r";
			$newData[] = $magic;
		}

		switch ($command) {
			case 'n':
				static::$page++;
				break;
			case 'p':
				static::$page--;
				break;
		}

		return $command;
	}

	/**
	 * @param $result
	 *
	 * @return string
	 */
	public static function replaceSingleLine($result)
	{
		return sprintf("\r%s", $result);
	}

	/**
	 * @param      $data
	 * @param null $lastLines
	 *
	 * @return string
	 */
	public static function replaceMultiLine($data, $lastLines = null)
	{
		if (!is_null($lastLines)) {
			static::$lastLines = $lastLines;
		}
		$data = PHP_EOL . $data;

		$termHeight = (int)exec('tput lines', $toss, $status);
		if ($status) {
			$termHeight = 64; // Arbitrary fall-back term width.
		}
		$termHeight = $termHeight - 1;

		$termWidth = (int)exec('tput cols', $toss, $status);

		if ($status) {
			$termWidth = 64; // Arbitrary fall-back term width.
		}

		$lineCount = 0;

		$temp = new \ArrayIterator(explode(PHP_EOL, $data));

		// Output max lines count
		foreach ($temp as $line) {
			$lineCount += count(str_split($line, $termWidth));
		}
		$temp->rewind();

		$newData = [];
		$linesInTop = $lineCount - $termHeight;
		if ($linesInTop > 0) {
			$pages = ceil($linesInTop / $termHeight);

			if (static::$page > $pages) {
				static::$page = 0;
			} elseif (static::$page < 0) {
				static::$page = $pages;
			}
			$position = static::$page * $termHeight;

			stream_set_blocking(STDIN, false);
			$command = fgetc(STDIN);
			if ($command) {
				$magic = "\r\033[K\033[1A\r\033[K\r";
				$newData[] = $magic;
			}

			if ($position > $temp->count()) {
				$position = 0;
			}

			$temp->seek($position);
			$i = 1;
			while ($temp->valid()) {
				if ($i > $termHeight) {
					break;
				}
				$newData[] = $temp->current();
				$i++;
				$temp->next();
			}
//            $newData[] = $position.'/'. static::$page.'/'.$pages;
		} else {
			stream_set_blocking(STDIN, false);
			$command = fgetc(STDIN);
			if ($command) {
				$magic = "\r\033[K\033[1A\r\033[K\r";
				$newData[] = $magic;
			}

			foreach ($temp as $line) {
				$newData[] = $line;
			}
		}

		$data = implode(PHP_EOL, $newData);

		$magic = '';
		// Erasure MAGIC: Clear as many lines as the last output had.
		for ($i = 0; $i < static::$lastLines; $i++) {
			// Return to the beginning of the line
			$magic .= "\r";
			// Erase to the end of the line
			$magic .= "\033[K";
			// Move cursor Up a line
			$magic .= "\033[1A";
			// Return to the beginning of the line
			$magic .= "\r";
			// Erase to the end of the line
			$magic .= "\033[K";
			// Return to the beginning of the line
			$magic .= "\r";
		}
		static::$lastLines = $lineCount;

		return $magic . $data . "\n";
	}

	/**
	 * @param string $message
	 * @param bool   $newline
	 *
	 * @return $this
	 */
	protected function write($message, $newline)
	{
		if ($newline) {
			$message = $message . PHP_EOL;
		}
		$this->temp .= $message;

		return $this;
	}

	/**
	 * @param string $message
	 *
	 * @return $this
	 */
	public function add($message)
	{
		$this->temp .= $message . PHP_EOL;

		return $this;
	}

	/**
	 * @return $this
	 */
	public function prepare()
	{
		$this->data .= $this->temp . PHP_EOL;

		return $this;
	}

	/**
	 * @return $this
	 */
	public function fetch()
	{
		echo LinesManipulator::replaceMultiLine($this->data, $this->lastLine);

		$this->lastLine = null;

		$this->clearTemp();

		$this->clearData();

		return $this;
	}

	/**
	 * @return $this
	 */
	public function clearTemp()
	{
		$this->temp = '';

		return $this;
	}

	/**
	 * @return $this
	 */
	public function clearData()
	{
		$this->data = '';

		return $this;
	}

	/**
	 * @return $this
	 */
	public function newOutput()
	{
		$this->lastLine = 1;

		return $this;
	}
}