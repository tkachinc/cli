<?php
namespace TkachInc\CLI\Output;

/**
 * Class Colors
 */
class ColorString
{
	/**
	 * @var array
	 */
	protected static $foregroundColors = [
		'black'        => '0;30',
		'dark_gray'    => '1;30',
		'blue'         => '0;34',
		'light_blue'   => '1;34',
		'green'        => '0;32',
		'light_green'  => '1;32',
		'cyan'         => '0;36',
		'light_cyan'   => '1;36',
		'red'          => '0;31',
		'light_red'    => '1;31',
		'purple'       => '0;35',
		'light_purple' => '1;35',
		'brown'        => '0;33',
		'yellow'       => '1;33',
		'light_gray'   => '0;37',
		'white'        => '1;37',
	];

	/**
	 * @var array
	 */
	protected static $backgroundColors = [
		'black'      => '40',
		'red'        => '41',
		'green'      => '42',
		'yellow'     => '43',
		'blue'       => '44',
		'magenta'    => '45',
		'cyan'       => '46',
		'light_gray' => '47',
	];

	/**
	 * @param string $string
	 * @param null   $foregroundColor
	 * @param null   $backgroundColor
	 *
	 * @return string
	 */
	public static function get($string, $foregroundColor = null, $backgroundColor = null)
	{
		$coloredString = "";

		// Check if given foreground color found
		if (isset(static::$foregroundColors[$foregroundColor])) {
			$coloredString .= "\033[" . static::$foregroundColors[$foregroundColor] . "m";
		}
		// Check if given background color found
		if (isset(static::$backgroundColors[$backgroundColor])) {
			$coloredString .= "\033[" . static::$backgroundColors[$backgroundColor] . "m";
		}

		// Add string and end coloring
		$coloredString .= $string . "\033[0m";

		return $coloredString;
	}

	/**
	 * @param $string
	 *
	 * @return string
	 */
	public static function error($string)
	{
		return static::get($string, 'red');
	}

	/**
	 * @param $string
	 *
	 * @return string
	 */
	public static function warning($string)
	{
		return static::get($string, 'brown');
	}

	/**
	 * @param $string
	 *
	 * @return string
	 */
	public static function success($string)
	{
		return static::get($string, 'green');
	}

	/**
	 * @param $string
	 *
	 * @return string
	 */
	public static function info($string)
	{
		return static::get($string, 'blue');
	}

	/**
	 * @param               $string
	 * @param               $type
	 * @param \Closure|null $function
	 *
	 * @return mixed
	 */
	public static function writeByType($string, $type, \Closure $function = null)
	{
		if ($function) {
			$type = call_user_func_array($function, [$string, $type]);
		}

		if (!method_exists(static::class, $type)) {
			$type = 'write';
		}

		return call_user_func_array([static::class, $type], [$string]);
	}

	/**
	 * @return array
	 */
	public static function getForegroundColors()
	{
		return array_keys(static::$foregroundColors);
	}

	/**
	 * @return array
	 */
	public static function getBackgroundColors()
	{
		return array_keys(static::$backgroundColors);
	}
}