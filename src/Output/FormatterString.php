<?php
namespace TkachInc\CLI\Output;

/**
 * Class Colors
 *
 * @package TkachInc\Core\Cli
 */
class FormatterString
{
	const DEFAULT_FILTER = "\033[0m";
	const TEXT_COLOR_BLACK = 30;
	const TEXT_COLOR_RED = 31;
	const TEXT_COLOR_GREEN = 32;
	const TEXT_COLOR_YELLOW = 33;
	const TEXT_COLOR_INDIGO = 34;
	const TEXT_COLOR_VIOLET = 35;
	const TEXT_COLOR_AQUAMARINE = 36;
	const TEXT_COLOR_GREY = 37;
	const BACKGROUND_COLOR_BLACK = 40;
	const BACKGROUND_COLOR_RED = 41;
	const BACKGROUND_COLOR_GREEN = 42;
	const BACKGROUND_COLOR_YELLOW = 43;
	const BACKGROUND_COLOR_INDIGO = 44;
	const BACKGROUND_COLOR_VIOLET = 45;
	const BACKGROUND_COLOR_AQUAMARINE = 46;
	const BACKGROUND_COLOR_GREY = 47;

	/**
	 * @var array
	 */
	protected $availableTextColor = [
		self::TEXT_COLOR_BLACK      => 1,
		self::TEXT_COLOR_RED        => 1,
		self::TEXT_COLOR_GREEN      => 1,
		self::TEXT_COLOR_YELLOW     => 1,
		self::TEXT_COLOR_INDIGO     => 1,
		self::TEXT_COLOR_VIOLET     => 1,
		self::TEXT_COLOR_AQUAMARINE => 1,
		self::TEXT_COLOR_GREY       => 1,
	];

	/**
	 * @var array
	 */
	protected $availableBackgroundColor = [
		self::BACKGROUND_COLOR_BLACK      => 1,
		self::BACKGROUND_COLOR_RED        => 1,
		self::BACKGROUND_COLOR_GREEN      => 1,
		self::BACKGROUND_COLOR_YELLOW     => 1,
		self::BACKGROUND_COLOR_INDIGO     => 1,
		self::BACKGROUND_COLOR_VIOLET     => 1,
		self::BACKGROUND_COLOR_AQUAMARINE => 1,
		self::BACKGROUND_COLOR_GREY       => 1,
	];

	/**
	 * @var string
	 */
	protected $string = '';

	/**
	 * @var string
	 */
	protected $filter = "\033[0";

	/**
	 * @var string
	 */
	protected static $default = "\033[0m";

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->getResultString();
	}

	/**
	 * @return $this
	 */
	public function bolt()
	{
		$this->filter .= ';1';

		return $this;
	}

	/**
	 * @return $this
	 */
	public function semiBright()
	{
		$this->filter .= ';2';

		return $this;
	}

	/**
	 * @return $this
	 */
	public function underlined()
	{
		$this->filter .= ';4';

		return $this;
	}

	/**
	 * @return $this
	 */
	public function flashing()
	{
		$this->filter .= ';5';

		return $this;
	}

	/**
	 * @return $this
	 */
	public function reversion()
	{
		$this->filter .= ';7';

		return $this;
	}

	/**
	 * @return $this
	 */
	public function setNormalIntensity()
	{
		$this->filter .= ';22';

		return $this;
	}

	/**
	 * @return $this
	 */
	public function cancelUnderscore()
	{
		$this->filter .= ';24';

		return $this;
	}

	/**
	 * @return $this
	 */
	public function cancelFlashing()
	{
		$this->filter .= ';25';

		return $this;
	}

	/**
	 * @return $this
	 */
	public function cancelReversion()
	{
		$this->filter .= ';25';

		return $this;
	}

	/**
	 * @param $color
	 *
	 * @return $this
	 */
	public function textColor($color)
	{
		if (isset($this->availableTextColor[$color])) {
			$this->filter .= ';' . $color;
		}

		return $this;
	}

	/**
	 * @param $color
	 *
	 * @return $this
	 */
	public function backgroundColor($color)
	{
		if (isset($this->availableBackgroundColor[$color])) {
			$this->filter .= ';' . $color;
		}

		return $this;
	}

	/**
	 * @param string $afterString
	 *
	 * @return string
	 */
	public function getResultString($afterString = '')
	{
		$this->result .= $this->filter . 'm' . $this->string . self::DEFAULT_FILTER . $afterString;

		return $this->result;
	}

	protected $result = '';

	protected $afterString = '';

	/**
	 * @param        $string
	 * @param string $afterString Not filtered string (\n and other)
	 *
	 * @return $this
	 */
	public function newString($string, $afterString = '')
	{
		$this->getResultString($this->afterString);
		$this->reset();
		$this->string = $string;
		$this->afterString = $afterString;

		return $this;
	}

	/**
	 * @return $this
	 */
	public function reset()
	{
		$this->filter = "\033[0";

		return $this;
	}

	/**
	 * @return $this
	 */
	public function resetResult()
	{
		$this->result = '';

		return $this;
	}
}