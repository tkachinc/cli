[![Codacy Badge](https://api.codacy.com/project/badge/Grade/c3c5bfe9a3bf4b3b9ac1202ca60bc449)](https://www.codacy.com/app/gollariel/cli?utm_source=tkachinc@bitbucket.org&amp;utm_medium=referral&amp;utm_content=tkachinc/cli&amp;utm_campaign=Badge_Grade)

## CLI

Contains CLI helpers. Include:
 - Info about process
 - CLI output manipulation
 - CLI argument parser